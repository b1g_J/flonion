# flonion
- Are you in need of a hidden services site? Do you know how to make a flask app? Well then you are in luck because flonion makes it very easy to deploy a dockerized flask web app as a hidden service.
- A big thanks to goldy for making a hidden services docker container!

# Running docker containers
docker-compose is great! all you need to do is run
```bash
docker-compose build && docker-compose up
```

# Testing
If you are just want to test the flask webapp, that is simple too! Run the run script, and it will automatically create a virtual enviorment, install all dependencies, then run on http://localhost:5000. 
```bash
./run.sh
```
